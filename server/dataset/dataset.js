const file_users = cat('users.json');
const file_clients = cat('clients.json');

const users = JSON.parse(file_users);
const clients = JSON.parse(file_clients);

users.forEach((user) => {
  user.register_time = new Date();
  db.user.insertOne(user);
});

clients.forEach((client) => {
  db.client.insertOne(client);
});
